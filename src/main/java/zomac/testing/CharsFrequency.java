package zomac.testing;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class CharsFrequency {

    private String filename;
    private Map<Character, Integer> charsMap;

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Wrong arguments");
            System.exit(1);
        }

        CharsFrequency charsFrequency = new CharsFrequency(args[0]);
        charsFrequency.run();
    }

    public CharsFrequency(String filename) {
        this.filename = filename;
        this.charsMap = new HashMap<>();
    }

    void run() {
        readFile();

        charsMap = charsMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        int total = charsMap.values().stream()
                .mapToInt(Integer::intValue).sum();

        writeToConsole(total);
    }

    private void readFile() {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(filename)) {
            Scanner scanner = new Scanner(input);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                for (char ch : line.toCharArray()) {
                    if (ch == ' ') {
                        continue;
                    }
                    if (charsMap.containsKey(ch)) {
                        charsMap.put(ch, charsMap.get(ch) + 1);
                    } else {
                        charsMap.put(ch, 1);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToConsole(int total) {
        for (Map.Entry<Character, Integer> entry : charsMap.entrySet()) {
            float percentage = (float)entry.getValue() * 100 / total;
            int hashCount = Math.round(percentage / 2);
            String histogram = new String(new char[hashCount]).replace("\0", "#");
            System.out.printf("%s (%.1f%%): %s\n", entry.getKey(), percentage, histogram);
        }
    }
}
